# Social networks wall

This module provides a block with a list containing data from social networks.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/social_wall).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/social_wall).


## Contents of this file

- Installation
- Requirements
- Configuration
- Examples
- Maintainers


## Installation

- Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Requirements

This module requires 2 external libraries (for Twitter & Instagram plugins),
that are automatically installed when requiring the module :

- `"abraham/twitteroauth": "^2"`
- `"pgrimaud/instagram-user-feed": "^6"`


## Configuration

1. Create new social network (optional, for a specific need) :
   - Create a new plugin in `[your_module_name]/src/Plugin/SocialNetwork` that
   extends `SocialNetworkBase`
   - Implement at least these functions:
     - `getLabel()` : returns the label of the social network
     - `settingsForm(array $settings = [])`: returns the configuration form
     of this social network
     - `render()`: returns the social network render array
   - Clear your cache ;)

2. Configure social networks:
   - Go to `admin/config/services/social-wall` (or Configuration > Web 
   services > Social networks) to configure existing social networks.
3. Display social wall block:
   Chose which social networks to display, and the order, in block
   configuration.
4. Override the display (optional):
   You can override block display by creating a `social-wall--block.html.twig`
  in your theme.


## Examples

This module contains 2 basic plugin examples in
 `social_wall/src/Plugin/SocialNetwork`:
- Twitter
- Instagram


## Maintainers

- Nicolas N - [NicociN](https://www.drupal.org/u/nicocin)
- Marc-Antoine Marty - [Martygraphie](https://www.drupal.org/u/martygraphie)
- sbruyas - [sbruyas](https://www.drupal.org/u/sbruyas)

Supporting organizations:

- Ecedi - [Ecedi](https://www.drupal.org/ecedi)
